package com.example.character_3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity2 extends AppCompatActivity {

    EditText et_input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        et_input = (EditText) findViewById(R.id.Input);

    }

    public void butonclick(View view){
        Intent intent = new Intent(MainActivity2.this,MainActivity.class);
        intent.putExtra("Input",et_input.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}