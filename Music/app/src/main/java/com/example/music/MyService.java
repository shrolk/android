package com.example.music;

import android.app.Service;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;


public class MyService extends Service {

    private MediaPlayer mediaPlayer;

    public MyService() {
        Log.e("TAG!!!","MyService()");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("TAG!!!","service_onCreate()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int i = super.onStartCommand(intent, flags, startId);
        String action = intent.getStringExtra("action");
        if(action!=null) {
            switch (action) {
                case "start":
                    mediaPlayer.start();
                    break;
                case "pause":
                    mediaPlayer.pause();
                    break;
                case "recycle":
                    mediaPlayer.setLooping(true);
                    break;
            }
        }
        Log.e("TAG!!!","onStartCommand()");
        return i;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        Log.v("TAG!!!","onBind");
        return new MyBinder();
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer){
        this.mediaPlayer = mediaPlayer;
    }

    public MediaPlayer getMediaPlayer(){
        return mediaPlayer;
    }

    class MyBinder extends Binder{


        public MyService getService(){
            Log.e("TAG!!!","getService()");
            return MyService.this;

        }

    }
}