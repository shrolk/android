package com.example.music;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    MediaPlayer mediaPlayer;
    AssetManager assetManager;
    AssetFileDescriptor fd;
    TextView show_time_left, show_time_right,now_play_name;
    public static int recycle_num = 0;
    SeekBar seekBar;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private boolean isChanging = false;//互斥变量，防止定时器与SeekBar拖动时进度冲突
    ImageView _start_pause, _before, _next, _recycle;
    int[] recycle;
    RecyclerView.LayoutManager layoutManager;

    private RecyclerView recyclerView;
    RecycleAdapter adapter;
    ArrayList<String> musiclist_name;
    int now_position = 0;
    int musicTime;

    //绑定Service
    MyService myService;
    public ServiceConnection conn;
    String now_name;
    boolean isBind = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e("TAG!!!", "Start");

        init();
        Intent init_intent = new Intent(this, MyService.class);
        bindService(init_intent, conn, Context.BIND_AUTO_CREATE);
        try {
            initEvent();
            initMusic();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //播放和暂停
        _start_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myService.setMediaPlayer(mediaPlayer);
                Intent intent = new Intent(MainActivity.this, MyService.class);
                if (!mediaPlayer.isPlaying()) {
                    intent.putExtra("action", "start");
                    _start_pause.setImageResource(R.drawable.start);
                } else {
                    intent.putExtra("action", "pause");
                    _start_pause.setImageResource(R.drawable.pause);
                }
                myService.startService(intent);
            }
        });

        //单曲循环，列表循环，单曲播放
        _recycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myService.setMediaPlayer(mediaPlayer);
                recycle_num++;
                _recycle.setImageResource(recycle[recycle_num % 3]);
                Intent intent = new Intent(MainActivity.this, MyService.class);
                if(recycle[recycle_num % 3] ==R.drawable.recycle_one) {
                        intent.putExtra("action", "recycle");
                }
                myService.startService(intent);
            }
        });

        //下一首
        _next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_next();
            }
        });

        //上一首
        _before.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_before();
            }
        });
    }

    //上一首
    public void set_before() {

        try {
            if (now_position ==0 ) {
                now_position =musiclist_name.size() - 1 ;
                now_name = musiclist_name.get(now_position);
            } else {
                now_position--;
                now_name = musiclist_name.get(now_position);
            }
            mediaPlayer.reset();
            initMusic();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //后一首
    public void set_next() {
        mediaPlayer.setLooping(false);
        switch (recycle[recycle_num % 3]) {
            case R.drawable.recycle_unorder://随机播放
                mediaPlayer.setLooping(false);
                seekBar.setProgress(0);
                now_position = new Random().nextInt(musiclist_name.size());
                now_name = musiclist_name.get(now_position);
                break;
            default://列表循环
                if (now_position ==musiclist_name.size() - 1) {
                    now_position = 0;
                    now_name = musiclist_name.get(now_position);
                } else {
                    now_position++;
                    now_name = musiclist_name.get(now_position);
                }
                break;
        }
        try {
            mediaPlayer.reset();
            initMusic();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //配置器初始化
    public void initEvent() throws IOException {
        recyclerView = findViewById(R.id.music_list);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    //初始化音乐列表
    public ArrayList<String> initMusic_list() throws IOException {
        ArrayList<String> musiclist = new ArrayList<>();
        String[] music = getAssets().list("");
        int flag = 0;
        String pattern = ".*mp3.*";
        for (String i : music) {
            if (Pattern.matches(pattern, i)) {
                musiclist.add(i);
            }
        }
        return musiclist;
    }

    private void init() {
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        seekBar.setOnSeekBarChangeListener(new MySeekbar());
        show_time_right = (TextView) findViewById(R.id.show_time_right);
        show_time_left = (TextView) findViewById(R.id.show_time_left);
        _start_pause = (ImageView) findViewById(R.id.picture_start_pause);
        _before = (ImageView) findViewById(R.id.picture_before);
        _next = (ImageView) findViewById(R.id.picture_next);
        _recycle = (ImageView) findViewById(R.id.picture_recycle_list);
        now_play_name = (TextView) findViewById(R.id.now_play_name);
        recycle = new int[]{R.drawable.recycle_list, R.drawable.recycle_unorder, R.drawable.recycle_one};
        try {
            musiclist_name = initMusic_list();
        } catch (IOException e) {
            e.printStackTrace();
        }
        conn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                isBind = true;
                MyService.MyBinder myBinder = (MyService.MyBinder) iBinder;
                myService = myBinder.getService();
                Log.e("TAG!!!", "onServiceConnected");
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                isBind = false;
            }
        };
        adapter = new RecycleAdapter(MainActivity.this, musiclist_name);
    }


    //刷新当前播放的歌曲
    public void updateMusic() {
        if (mediaPlayer != null) {
            now_play_name.setText("正在播放的是："+musiclist_name.get(now_position));
            seekBar.setMax(mediaPlayer.getDuration());
            mTimer = new Timer();
            mTimerTask = new TimerTask() {
                @Override
                public void run() {
                    if (isChanging == true) {
                        return;
                    }
                    seekBar.setProgress(mediaPlayer.getCurrentPosition());

                }
            };
            mTimer.schedule(mTimerTask, 0, 10);
            if (mediaPlayer.isPlaying()) {
                _start_pause.setImageResource(R.drawable.start);
            } else
                _start_pause.setImageResource(R.drawable.pause);
            musicTime = mediaPlayer.getDuration() / 1000;
            if (musicTime % 60 < 10)
                show_time_right.setText(musicTime / 60 + ":0" + musicTime % 60);
            else
                show_time_right.setText(musicTime / 60 + ":" + musicTime % 60);
            Log.e("TAG!!!", "updateMusic()");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
        mediaPlayer.release();
    }


    //进度条处理
    class MySeekbar implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            int time = seekBar.getProgress() / 1000;
            if (time % 60 < 10) {
                show_time_left.setText(time / 60 + ":0" + time % 60);
            } else
                show_time_left.setText(time / 60 + ":" + time % 60);
            if (seekBar.getProgress() == seekBar.getMax()) {
                Log.d("TAG", "seekBar.getProgress() == seekBar.getMax()...");
                if(recycle[recycle_num % 3] != R.drawable.recycle_one){
                    mediaPlayer.setLooping(false);
                    seekBar.setProgress(0);
                    set_next();
                }
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            isChanging = true;
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mediaPlayer.seekTo(seekBar.getProgress());
            isChanging = false;
        }
    }

    //初始化播放音乐
    public void initMusic() throws IOException {
        if (now_name != null) {
            fd = assetManager.openFd(now_name);
        } else {
            assetManager = getAssets();
            fd = assetManager.openFd(musiclist_name.get(now_position));
        }
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(fd.getFileDescriptor(), fd.getStartOffset(), fd.getLength());
        mediaPlayer.prepare();

        if (now_name != null)
            mediaPlayer.start();
        updateMusic();
        Log.v("TAG!!!", "initMusic()");
    }

    class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.MyViewHolder> {

        private List<String> result_list;
        private LayoutInflater rLayoutInflater;
        private Context rContext;

        public RecycleAdapter(Context context, List<String> musiclist) {
            rContext = context;
            result_list = musiclist;
            rLayoutInflater = rLayoutInflater.from(rContext);
        }

        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = rLayoutInflater.inflate(R.layout.music_card, parent, false);
            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            String name = result_list.get(position);
            holder.tv_Music.setText(name);
            holder.tv_Music.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("TAG!!!", "ItemHolder");
                    mediaPlayer.reset();
                    now_name = name;
                    now_position = position;
                    try {
                        initMusic();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            holder.tv_Music.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("删除歌曲");

                    builder.setMessage("您确定要删除歌曲" + musiclist_name.get(position))
                            .setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    musiclist_name.remove(position);
                                    try {
                                        initEvent();
                                        Toast.makeText(MainActivity.this, "删除成功", Toast.LENGTH_SHORT).show();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .create()
                            .show();
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return result_list.size();
        }


        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_Music;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                this.tv_Music = itemView.findViewById(R.id.music_name);
            }
        }
    }


}





