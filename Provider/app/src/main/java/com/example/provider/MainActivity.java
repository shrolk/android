package com.example.provider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.service.controls.templates.TemperatureControlTemplate;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    EditText tv_search_delete,add_word,add_meaning,add_sentence,
            update_word,update_meaning,update_sentence;
    TextView ot_word,ot_meaning,ot_sentence;
    private int URI_WORDS = 2;
    private int URI_WORDS_ITEM = 1;
    private String AUTHORITY = "com.example.vocabulary_book.provider.DatabaseProvider";
    String wordId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_search_delete = (EditText) findViewById(R.id.search_edit);
        add_word = (EditText) findViewById(R.id.add_word_edit);
        add_meaning=(EditText) findViewById(R.id.add_meaning_edit);
        add_sentence=(EditText) findViewById(R.id.add_sentenc_edit);
        update_word = (EditText) findViewById(R.id.update_word_edit);
        update_meaning = (EditText) findViewById(R.id.update_meaning_edit);
        update_sentence = (EditText) findViewById(R.id.update_sentence_edit);
        ot_word = (TextView) findViewById(R.id.output_word);
        ot_meaning = (TextView) findViewById(R.id.output_meaning);
        ot_sentence = (TextView) findViewById(R.id.output_sentenc);
    }

    //查询按钮
    public void Button_search(View view){
        int n=0;
        String str = tv_search_delete.getText()+"";
        Uri uri = Uri.parse("content://"+AUTHORITY+"/" + Words.Word.TABLE_NAME);
        Cursor wordCursor = getContentResolver().query(uri,null,null,null,null);
        if(wordCursor!=null){
            while(wordCursor.moveToNext()){
                int word = wordCursor.getColumnIndex(Words.Word.COLUMN_NAME_WORD);
                int meaning = wordCursor.getColumnIndex(Words.Word.COLUMN_NAME_MEANING);
                int sentence = wordCursor.getColumnIndex(Words.Word.COLUMN_NAME_SAMPLE);
                if(wordCursor.getString(word).equals(str)){
                    ot_word.setText("单词:"+wordCursor.getString(word));
                    ot_meaning.setText("释义:"+wordCursor.getString(meaning));
                    ot_sentence.setText("例句:" + wordCursor.getString(sentence));
                    Toast.makeText(MainActivity.this, "查询成功", Toast.LENGTH_SHORT).show();
                    n++;
                    break;
                }
            }
            if(n==0)
                Toast.makeText(MainActivity.this, "该单词不存在", Toast.LENGTH_SHORT).show();
        }

    }
    //删除按钮
    public void Button_delete(View view){
        String str = tv_search_delete.getText()+"";
        Uri uri = Uri.parse("content://"+AUTHORITY+"/"+ Words.Word.TABLE_NAME);
        int result = getContentResolver().delete(uri, Words.Word.COLUMN_NAME_WORD+"=?",new String[]{str});
        if(result!=0)
            Toast.makeText(MainActivity.this, "删除成功", Toast.LENGTH_SHORT).show();
    }

    //添加按钮
    public void Button_add(View view){

        ContentValues contentValues = new ContentValues();
        contentValues.put(Words.Word.COLUMN_NAME_WORD,add_word.getText()+"");
        contentValues.put(Words.Word.COLUMN_NAME_MEANING,add_meaning.getText()+"");
        contentValues.put(Words.Word.COLUMN_NAME_SAMPLE,add_sentence.getText()+"");
        Uri uri = Uri.parse("content://"+AUTHORITY+"/"+ Words.Word.TABLE_NAME);
        Uri new_uri = getContentResolver().insert(uri,contentValues);
        wordId = new_uri.getLastPathSegment();
        if(wordId!=null)
            Toast.makeText(MainActivity.this, "添加成功", Toast.LENGTH_SHORT).show();
    }

    //修改获取按钮
    public void Button_get(View view){
        int n=0;
        String str = update_word.getText()+"";
        Uri uri = Uri.parse("content://"+AUTHORITY+"/" + Words.Word.TABLE_NAME);
        Cursor wordCursor = getContentResolver().query(uri,null,null,null,null);
        if(wordCursor!=null){
            while(wordCursor.moveToNext()){
                int word = wordCursor.getColumnIndex(Words.Word.COLUMN_NAME_WORD);
                int meaning = wordCursor.getColumnIndex(Words.Word.COLUMN_NAME_MEANING);
                int sentence = wordCursor.getColumnIndex(Words.Word.COLUMN_NAME_SAMPLE);
                if(wordCursor.getString(word).equals(str)){
                    update_word.setText(wordCursor.getString(word));
                    update_word.setFocusableInTouchMode(false);
                    update_meaning.setText(wordCursor.getString(meaning));
                    update_sentence.setText(wordCursor.getString(sentence));
                    n++;
                    break;
                }
            }
            if(n==0)
                Toast.makeText(MainActivity.this, "该单词不存在", Toast.LENGTH_SHORT).show();
        }
    }

    //修改确认按钮
    public void Button_update(View view){
        String str = update_word.getText()+"";
        Uri uri = Uri.parse("content://"+AUTHORITY+"/"+ Words.Word.TABLE_NAME);
        int result = getContentResolver().delete(uri, Words.Word.COLUMN_NAME_WORD+"=?",new String[]{str});
        ContentValues contentValues = new ContentValues();
        contentValues.put(Words.Word.COLUMN_NAME_WORD,update_word.getText()+"");
        contentValues.put(Words.Word.COLUMN_NAME_MEANING,update_meaning.getText()+"");
        contentValues.put(Words.Word.COLUMN_NAME_SAMPLE,update_sentence.getText()+"");
        Uri uri1 = Uri.parse("content://"+AUTHORITY+"/"+ Words.Word.TABLE_NAME);
        Uri new_uri = getContentResolver().insert(uri1,contentValues);
        wordId = new_uri.getLastPathSegment();

        if(wordId!=null)
            Toast.makeText(MainActivity.this, "修改成功", Toast.LENGTH_SHORT).show();
    }
}
