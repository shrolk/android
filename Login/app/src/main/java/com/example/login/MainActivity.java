package com.example.login;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static String TAG="LIFTCYCLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG,"(1)onCreate()");
    }

    @Override
    public void onStart(){
        super.onStart();
        Log.i(TAG,"(2) onStart()");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.i(TAG,"(3) onDestroy");
    }

}