package com.example.caculator;


import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Caculator {
    private String start;
    private String fin;
    List<Character> operation = new ArrayList<>();

    Caculator(String start) {
        operation.add('+');
        operation.add('-');
        operation.add('*');
        operation.add('/');
        operation.add('^');
        operation.add('(');
        operation.add(')');
        this.start = start;
        midPostback(this.start);
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    //计算
    public double goCaculate(String str) {
        double num1, num2;
        Stack<Double> num = new Stack<>();
        String dou = "";
        try {
            char[] cha = str.toCharArray();
            for (int i = 0; i < cha.length; i++) {
                if(cha[i]=='@'){
                    dou+='-';
                    continue;
                }
                if (operation.contains(cha[i])) {
                    num1 = num.pop();
                    num2 = num.pop();
                    num.push(Count(num1, num2, cha[i]));
                } else {
                    while (cha[i] != ';') {
                        dou += cha[i];
                        i++;
                    }
                    num.push(Double.parseDouble(dou));
                    dou = "";
                }
            }
            return num.pop();
        } catch (Exception e) {
            e.printStackTrace();
            return -0.0;
        }
    }

    //幂运算
    public double Power(double x, double y) {
        int result = 1;
        for (int i = 0; i < (int) y; i++)
            result *= (int) x;
        return (double) result;
    }

    //进行算数运算
    public double Count(double num1, double num2, char oper) {
        switch (oper) {
            case '+':
                return num2 + num1;
            case '-':
                return num2 - num1;
            case '*':
                return num2 * num1;
            case '/':
                return num2 / num1;
            case '^':
                return Power(num2, num1);
        }
        return Double.parseDouble(null);
    }

    //中缀表达式转后缀表达式
    public String midPostback(String stri) {
        String str = "";
        String end = "";
        try {
            char[] cha = stri.toCharArray();
            int peek = 0;
            int now = 0;
            Stack<Character> opr = new Stack<Character>();
            for (int i = 0; i < cha.length; i++) {
                if (i == 0)
                    if (cha[i] == '-') {
                        end = end + '@';
                        continue;
                    }
                if (cha[i] == '(') {
                    opr.push(cha[i]);
                    if (cha[i + 1] == '-') {
                        end += '@';
                        i = i + 1;
                    }
                    continue;
                } else if (cha[i] == ')') {
                    while (opr.peek() != '(') {
                        end += opr.pop();
                    }
                    opr.pop();
                    continue;
                }
                if (!operation.contains(cha[i])) {
                    str += cha[i];
                    if (i + 1 < start.length() && operation.contains(cha[i + 1])) {
                        end = end + str + ";";
                        str = "";
                    }
                } else {
                    if (opr.empty())
                        opr.push(cha[i]);
                    else {
                        peek = oper_sort(opr.peek());
                        now = oper_sort(cha[i]);
                        if (now > peek)
                            opr.push(cha[i]);
                        else {
                            while (!opr.empty() && (!(oper_sort(opr.peek()) < oper_sort(cha[i])))) {
                                end += opr.pop();
                            }
                            opr.push(cha[i]);
                        }
                    }
                }

            }
            if (!str.equals(""))
                end += str + ";";
            while (!opr.empty())
                end += opr.pop();
            this.fin = end;
            return end;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int oper_sort(char cha) {
        if (cha == '^')
            return 3;
        else if (cha == '*' || cha == '/')
            return 2;
        else if (cha == '+' || cha == '-')
            return 1;
        else
            return 0;
    }
}
