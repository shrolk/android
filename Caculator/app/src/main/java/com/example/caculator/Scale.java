package com.example.caculator;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Scale extends AppCompatActivity {
    TextView tv_input, tv_output;
    Button bt_yes, bt_cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scale);

        Spinner spinner_start = (Spinner) findViewById(R.id.JinZ_Spinner_First);
        ArrayAdapter<CharSequence> adapter_start = ArrayAdapter.createFromResource(this, R.array.items, android.R.layout.simple_spinner_item);
        adapter_start.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_start.setAdapter(adapter_start);

        spinner_start.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Spinner spinner_end = (Spinner) findViewById(R.id.JinZ_Spinner_End);
        ArrayAdapter<CharSequence> adapter_end = ArrayAdapter.createFromResource(this, R.array.items, android.R.layout.simple_spinner_item);
        adapter_end.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_end.setAdapter(adapter_end);

        spinner_end.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        bt_yes = findViewById(R.id.yes);
        bt_cancel = findViewById(R.id.cancel);

        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String str_first = spinner_start.getSelectedItem().toString();
                    String str_end = spinner_end.getSelectedItem().toString();
                    transform(str_first, str_end);
                } catch (Exception e) {
                    tv_input.setText("");
                    AlertDialog.Builder builder = new AlertDialog.Builder(Scale.this);
                    builder.setMessage("文本输入有误，请重新输入").setTitle("提示");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    builder.show();
                    return;
                }
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Scale.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }

    public void transform(String str_first, String str_end) {
        tv_input = (TextView) findViewById(R.id.Input);
        tv_output = (TextView) findViewById(R.id.Output);
        if (str_first.equals(str_end)) {
            tv_output.setText(tv_input.getText());
            return;
        } else if (str_first.equals("十进制")) {
            switch (str_end) {
                case "二进制":
                    tv_output.setText(Integer.toBinaryString(Integer.parseInt(tv_input.getText() + "")));
                    break;
                case "八进制":
                    tv_output.setText(Integer.toOctalString(Integer.parseInt(tv_input.getText() + "")));
                    break;
                case "十六进制":
                    tv_output.setText(Integer.toHexString(Integer.parseInt(tv_input.getText() + "")));
            }
            return;
        } else if (str_first.equals("二进制")) {
            switch (str_end) {
                case "十进制":
                    tv_output.setText(Integer.valueOf(tv_input.getText() + "", 2).toString());
                    break;
                case "八进制":
                    tv_output.setText(Integer.toOctalString(Integer.parseInt(tv_input.getText() + "", 2)));
                    break;
                case "十六进制":
                    tv_output.setText(Integer.toHexString(Integer.parseInt(tv_input.getText() + "", 2)));
            }
            return;
        } else if (str_first.equals("八进制")) {
            switch (str_end) {
                case "二进制":
                    tv_output.setText(Integer.toBinaryString(Integer.valueOf(tv_input.getText() + "", 8)));
                    break;
                case "十进制":
                    tv_output.setText(Integer.valueOf(tv_input.getText() + "", 8).toString());
                    break;
                case "十六进制":
                    tv_output.setText(Integer.toHexString(Integer.valueOf(tv_input.getText() + "", 8)));
            }
            return;
        } else if (str_first.equals("十六进制")) {
            String str = tv_input.getText() + "";
            switch (str_end) {
                case "二进制":
                    tv_output.setText(Integer.toBinaryString(Integer.valueOf(tv_input.getText() + "", 16)));
                    break;
                case "八进制":
                    tv_output.setText(Integer.toOctalString(Integer.valueOf(tv_input.getText() + "", 16)));
                    break;
                case "十进制":
                    tv_output.setText(Integer.valueOf(tv_input.getText() + "", 16).toString());
            }
            return;
        }
    }

}