package com.example.caculator;

public class Change_System {
    private double Number1;
    private String Choice;

    public double getNumber1() {
        return Number1;
    }

    public void setNumber1(double number1) {
        Number1 = number1;
    }


    public String getChoice() {
        return Choice;
    }

    public void setChoice(String choice) {
        Choice = choice;
    }

    Change_System(double Number1, String Choice) {
        this.Number1 = Number1;
        this.Choice = Choice;
    }

    //特殊运算
    public String[] special() {
        switch (this.Choice) {
            case "Change_M":
                return Change_M();
            case "Volume":
                return Change_TJ();
            case "Fuction":
                return fuction_Math();
            case "Scale":
                return Scale();
        }
        return null;
    }

    //米制单位换算
    public String[] Change_M() {
        int j = 0;
        String[] result = new String[6];
        for (int i = 0; i < 6; i = i + 2) {
                result[i] = Double.toString(Number1 / (Math.pow(10,j)));
                j++;
        }
        result[1] = "厘米 ";
        result[3] = "分米 ";
        result[5] = "米 ";
        return result;
    }

    //体积单位换算
    public String[] Change_TJ() {
        int j = 0;
        String[] result = new String[6];
        for (int i = 0; i < 6; i = i + 2) {
                result[i] = Double.toString(Number1 / (Math.pow(1000,j)));
                j++;
        }
        result[1] = "立方厘米 ";
        result[3] = "立方分米 ";
        result[5] = "立方米 ";
        return result;
    }

    //基本三角函数求解
    public String[] fuction_Math() {
        String[] result = new String[6];
        result[0] = "Sin:";
        result[1] = String.format("%.3f", Math.sin(Number1 * Math.PI / 180));
        result[2] = "Cos:";
        result[3] = String.format("%.3f", Math.cos(Number1 * Math.PI / 180));
        result[4] = "Tan:";
        result[5] = String.format("%.3f", Math.tan(Number1 * Math.PI / 180));
        return result;
    }

    //进制转换
    public String[] Scale() {
        String[] result = new String[6];
        int str = (int) Number1;
        result[0] = "二进制:";
        result[1] = Integer.toBinaryString(str);
        result[2] = "八进制:";
        result[3] = Integer.toOctalString(str);
        result[4] = "十六进制:";
        result[5] = Integer.toHexString(str);
        return result;
    }
}
