package com.example.caculator;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ServiceConfigurationError;
import java.util.Set;
import java.util.concurrent.Callable;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvResult, tvSpecial;
    String sign = "";
    boolean bol_log = false,bol_JieChen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvResult = findViewById(R.id.Final_Result);
        SetListener();
        tvResult.setText("");
    }

    public void SetListener() {
        findViewById(R.id.Number_0).setOnClickListener(this);
        findViewById(R.id.Number_1).setOnClickListener(this);
        findViewById(R.id.Number_2).setOnClickListener(this);
        findViewById(R.id.Number_3).setOnClickListener(this);
        findViewById(R.id.Number_4).setOnClickListener(this);
        findViewById(R.id.Number_5).setOnClickListener(this);
        findViewById(R.id.Number_6).setOnClickListener(this);
        findViewById(R.id.Number_7).setOnClickListener(this);
        findViewById(R.id.Number_8).setOnClickListener(this);
        findViewById(R.id.Number_9).setOnClickListener(this);
        findViewById(R.id.Sign_left_team).setOnClickListener(this);
        findViewById(R.id.Sign_right_team).setOnClickListener(this);
        findViewById(R.id.Sign_Power).setOnClickListener(this);
        findViewById(R.id.Sign_Chu).setOnClickListener(this);
        findViewById(R.id.Sign_Back).setOnClickListener(this);
        findViewById(R.id.Sign_Chen).setOnClickListener(this);
        findViewById(R.id.Clear).setOnClickListener(this);
        findViewById(R.id.Sign_Sub).setOnClickListener(this);
        findViewById(R.id.Sign_Sum).setOnClickListener(this);
        findViewById(R.id.Sign_Exit).setOnClickListener(this);
        findViewById(R.id.Sign_Point).setOnClickListener(this);
        findViewById(R.id.Sign_Result).setOnClickListener(this);
        findViewById(R.id.Change_Scale).setOnClickListener(this);
        findViewById(R.id.Log).setOnClickListener(this);
        findViewById(R.id.JieChen).setOnClickListener(this);
        tvSpecial = findViewById(R.id.Text_Special);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Number_0:
                tvResult.setText(tvResult.getText() + "0");
                break;
            case R.id.Number_1:
                tvResult.setText(tvResult.getText() + "1");
                break;
            case R.id.Number_2:
                tvResult.setText(tvResult.getText() + "2");
                break;
            case R.id.Number_3:
                tvResult.setText(tvResult.getText() + "3");
                break;
            case R.id.Number_4:
                tvResult.setText(tvResult.getText() + "4");
                break;
            case R.id.Number_5:
                tvResult.setText(tvResult.getText() + "5");
                break;
            case R.id.Number_6:
                tvResult.setText(tvResult.getText() + "6");
                break;
            case R.id.Number_7:
                tvResult.setText(tvResult.getText() + "7");
                break;
            case R.id.Number_8:
                tvResult.setText(tvResult.getText() + "8");
                break;
            case R.id.Number_9:
                tvResult.setText(tvResult.getText() + "9");
                break;
            case R.id.Sign_Point:
                tvResult.setText(tvResult.getText() + ".");
                break;
            case R.id.Sign_Chen:
                tvResult.setText(tvResult.getText() + "*");
                break;
            case R.id.Sign_Chu:
                tvResult.setText(tvResult.getText() + "/");
                break;
            case R.id.Sign_left_team:
                tvResult.setText(tvResult.getText() + "(");
                break;
            case R.id.Sign_right_team:
                tvResult.setText(tvResult.getText() + ")");
                break;
            case R.id.Sign_Sum:
                tvResult.setText(tvResult.getText() + "+");
                break;
            case R.id.Sign_Sub:
                tvResult.setText(tvResult.getText() + "-");
                break;
            case R.id.Sign_Power:
                tvResult.setText(tvResult.getText() + "^");
                break;
            case R.id.Clear:
                tvResult.setText("");
                break;
            case R.id.Sign_Back:
                String str = (String) tvResult.getText();
                str = str.substring(0, str.length() - 1);
                tvResult.setText(str);
                break;
            case R.id.Sign_Exit:
//                android.os.Process.killProcess(android.os.Process.myPid());
                finish();
                break;
            case R.id.Sign_Result:
                if(sign.equals("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("请在最上方选择您的计算类型").setTitle("提示");
                    builder.setPositiveButton("确定",new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    builder.show();
                    return;
                }
                Caculator cac = new Caculator(tvResult.getText() + "");
                double end = cac.goCaculate(cac.getFin());
                if (end == -0.0) {
                    tvResult.setText("");
                    Toast.makeText(this, "文本输入有误，请重新输入", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!sign.equals("Change_Count")) {
                    Change_System cs = new Change_System(Double.parseDouble(tvResult.getText() + ""), sign);
                    String[] special = cs.special();
                    tvSpecial.setText("");
                    for (int i = 0; i < 6; i++) {
                        tvSpecial.setText(tvSpecial.getText() + special[i]);
                        if (i % 2 == 1)
                            tvSpecial.setText((tvSpecial.getText() + "\n"));
                    }
                    return;
                }
                if(bol_log){
                    double dou_log = Double.parseDouble(tvResult.getText()+"");
                    dou_log = Math.log(dou_log);
                    tvResult.setText(dou_log+"");
                    bol_log = false;
                    return ;
                }
                if(bol_JieChen){
                    double dou_log = Double.parseDouble(tvResult.getText()+"");
                    int end_Jie = (int)dou_log;
                    int end_end = 1;
                    for(int i = 1;i<end_Jie+1;i++){
                        end_end*=i;
                    }
                    tvResult.setText(end_end+"");
                    bol_JieChen = false;
                    return;
                }
                tvResult.setText("" + end);
                break;
            case R.id.Log:
                bol_log = true;
                break;
            case R.id.JieChen:
                bol_JieChen = true;
                break;
            case R.id.Change_Scale://跳转进制转换页面
                Intent intent = new Intent(MainActivity.this,Scale.class);
                startActivity(intent);
                break;
        }
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.Change_Count:
                if (checked)
                    sign = "Change_Count";
                break;
            case R.id.Change_M://长度单位换算
                if (checked)
                    sign = "Change_M";
                break;
            case R.id.Change_Volume://体积单位换算
                if (checked)
                    sign = "Volume";
                break;
            case R.id.Fuction://三角函数计算
                if (checked)
                    sign = "Fuction";
                break;
        }
    }
}