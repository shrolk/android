package com.example.vocabulary_book;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Map;

public class RightFragment extends Fragment {

    TextView tv_word, tv_meaning, tv_sentence;
    LayoutInflater inflater;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_right, container, false);
        tv_word = view.findViewById(R.id.right_output_word);
        tv_meaning = view.findViewById(R.id.right_output_meaning);
        tv_sentence = view.findViewById(R.id.right_output_sentence);
        Log.v("TAG","我执行Create方法了！！！！！！！！！！！！！！！！！！！！！！！！！！！！");
        return view;
    }

    public RightFragment(){
    }

    public void updateView( Map<String, String> map) {
        tv_word.setText(map.get(Words.Word.COLUMN_NAME_WORD));
        tv_meaning.setText(map.get(Words.Word.COLUMN_NAME_MEANING));
        tv_sentence.setText(map.get(Words.Word.COLUMN_NAME_SAMPLE));
    }


}