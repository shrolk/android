package com.example.vocabulary_book;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.MyViewHolder> {//构造适配器

    private List<Map<String, String>> result_list;
    private LayoutInflater rLayoutInflater;
    private Context rContext;
    private RightFragment rightFragment;
    Control ct;

    public RecycleAdapter(Context rConcext, List<Map<String, String>> result_list,RightFragment rightFragment) {
        this.rContext = rConcext;
        this.result_list = result_list;
        rLayoutInflater = rLayoutInflater.from(rContext);
        this.rightFragment = rightFragment;
        ct = new Control(rContext);
    }

    @Override
    public int getItemCount() {
        return result_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = rLayoutInflater.inflate(R.layout.word_card, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Map<String, String> map = result_list.get(position);
        holder.tv_Card.setText(map.get(Words.Word.COLUMN_NAME_WORD));

        holder.tv_Card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                PopupMenu popupMenu = new PopupMenu(rContext, view);
                MenuInflater inflater = popupMenu.getMenuInflater();
                inflater.inflate(R.menu.item_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.menu_delete:
                                AlertDialog.Builder delete = new AlertDialog.Builder(rContext);
                                delete.setTitle("删除单词");

                                delete.setMessage("您确定要删除单词" + map.get(Words.Word.COLUMN_NAME_WORD) + "吗?")
                                        .setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                            }
                                        })
                                        .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                int result = ct.delete_Word(map.get(Words.Word.COLUMN_NAME_WORD));
                                                if (result == 1) {
                                                    Toast.makeText(rContext, "删除成功", Toast.LENGTH_SHORT).show();
                                                } else
                                                    Toast.makeText(rContext, "删除失败", Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .create()
                                        .show();
                                break;
                            case R.id.menu_update:
                                AlertDialog.Builder update = new AlertDialog.Builder(rContext);
                                update.setTitle("修改单词");

                                LayoutInflater inflater = LayoutInflater.from(rContext);
                                View v = inflater.inflate(R.layout.add_dialog, null);
                                EditText add_word = (EditText) v.findViewById(R.id.Add_dialog_add);
                                EditText add_meaning = (EditText) v.findViewById(R.id.Add_dialog_meaning);
                                EditText add_sentence = (EditText) v.findViewById(R.id.Add_dialog_centence);
                                add_word.setText(map.get(Words.Word.COLUMN_NAME_WORD));
                                add_word.setFocusableInTouchMode(false);//不可改变单词的拼写
                                add_meaning.setText(map.get(Words.Word.COLUMN_NAME_MEANING));
                                add_sentence.setText(map.get(Words.Word.COLUMN_NAME_SAMPLE));
                                update.setView(v)
                                        .setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                            }
                                        })
                                        .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                int result = ct.update_Word(new Word(map.get(Words.Word.COLUMN_NAME_WORD), add_meaning.getText() + "", add_sentence.getText() + ""));
                                                if (result == 1) {
                                                    Toast.makeText(rContext, "修改成功", Toast.LENGTH_SHORT).show();
                                                }
                                                else
                                                    Toast.makeText(rContext, "修改失败", Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .create()
                                        .show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
                return false;
            }
        });

        //点击进行单词展示界面
        holder.tv_Card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Configuration mConfiguration = rContext.getResources().getConfiguration();
                int ori = mConfiguration.orientation;
                if (ori == Configuration.ORIENTATION_LANDSCAPE) {//在横屏状态下
                    rightFragment.updateView(map);
                } else {//竖屏状态下
                    Intent intent = new Intent(rContext, Card_Activity.class);
                    intent.putExtra(Words.Word.COLUMN_NAME_WORD, map.get(Words.Word.COLUMN_NAME_WORD));
                    intent.putExtra(Words.Word.COLUMN_NAME_MEANING, map.get(Words.Word.COLUMN_NAME_MEANING));
                    intent.putExtra(Words.Word.COLUMN_NAME_SAMPLE, map.get(Words.Word.COLUMN_NAME_SAMPLE));
                    rContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        Button tv_Card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            this.tv_Card = itemView.findViewById(R.id.Card_textview);

        }
    }
}


