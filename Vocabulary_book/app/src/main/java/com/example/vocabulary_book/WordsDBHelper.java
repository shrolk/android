package com.example.vocabulary_book;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.UserDictionary;
import android.util.Log;


import androidx.annotation.Nullable;

public class WordsDBHelper extends SQLiteOpenHelper {

    private final static  String DATABASE_NAME = "wordsdb";//数据库的名字
    private final static int DATABASE_VERSION = 1;//数据库版本

    //建表SQL
    private final static String SQL_CREATE_DATABASE="create table " + Words.Word.TABLE_NAME+"("+
            Words.Word.COLUMN_NAME_WORD+" string"+" primary key"+
            ","+Words.Word.COLUMN_NAME_MEANING+" string"+","+Words.Word.COLUMN_NAME_SAMPLE+" string" +")";

    //删表SQL
    private final static String SQL_DELETE_DATABASE ="DROP TABLE IF EXISTS" + Words.Word.TABLE_NAME;


    public WordsDBHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        Log.d("TAG","HelperCreate");
    }

    //创建数据库
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_DATABASE);
    }

    //当数据库升级时被调用，首先删除旧表，然后调用OnCreate()创建新表
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


}
