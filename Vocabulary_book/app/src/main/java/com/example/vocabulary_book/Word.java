package com.example.vocabulary_book;

public class Word {
    private String name;
    private String meaning;
    private String contence;

    public Word(String name,String meaning,String contence){
        this.name = name;
        this.meaning = meaning;
        this.contence = contence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getContence() {
        return contence;
    }

    public void setContence(String contence) {
        this.contence = contence;
    }

}
