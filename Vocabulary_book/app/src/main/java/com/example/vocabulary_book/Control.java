package com.example.vocabulary_book;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Control extends AppCompatActivity {
    WordsDBHelper mDbHelper;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    Control(Context context){
        mDbHelper = new WordsDBHelper(context);
    }

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        mDbHelper.close();
        super.onDestroy();
    }

    //添加单词
    public int add_Word(Word word){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Words.Word.COLUMN_NAME_WORD,word.getName());
        values.put(Words.Word.COLUMN_NAME_MEANING,word.getMeaning());
        values.put(Words.Word.COLUMN_NAME_SAMPLE,word.getContence());

        long newRowId = db.insert(Words.Word.TABLE_NAME,null,values);
        return (int)newRowId;
    }

    //获取全部单词
    public List<Map<String,String>> search_Word_All(){
        List<Map<String,String>> wordList = new ArrayList<>();
        Map<String,String> map_null = new HashMap<>();
        map_null.put("option","null");

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.query(Words.Word.TABLE_NAME,null,null,null,null,null,null);
        int column_word = cursor.getColumnIndex(Words.Word.COLUMN_NAME_WORD);
        int column_meaning = cursor.getColumnIndex(Words.Word.COLUMN_NAME_MEANING);
        int column_sentence = cursor.getColumnIndex(Words.Word.COLUMN_NAME_SAMPLE);
        if(cursor!=null){
            while(cursor.moveToNext()){
                Map<String,String> map = new HashMap<>();
                map.put(Words.Word.COLUMN_NAME_WORD,cursor.getString(column_word));
                map.put(Words.Word.COLUMN_NAME_MEANING,cursor.getString(column_meaning));
                map.put(Words.Word.COLUMN_NAME_SAMPLE,cursor.getString(column_sentence));
                wordList.add(map);
            }
            cursor.close();
        }
        return wordList;
    }

    //查询指定单词
    public Word search_Word(String name){

        Word signl_word;

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.query(Words.Word.TABLE_NAME,null,"word=?",new String[] {name},null,null,null);
        int column_word = cursor.getColumnIndex(Words.Word.COLUMN_NAME_WORD);
        int column_meaning = cursor.getColumnIndex(Words.Word.COLUMN_NAME_MEANING);
        int column_sentence = cursor.getColumnIndex(Words.Word.COLUMN_NAME_SAMPLE);
        if(cursor!=null){
            while(cursor.moveToNext()){
                signl_word = new Word(cursor.getString(column_word),cursor.getString(column_meaning),cursor.getString(column_sentence));
                cursor.close();
                return signl_word;
            }
        }
        return signl_word = new Word("1","1","1");
    }

    //模糊查询
    public List<Map<String,String>> search_fuzzy(String name){

        List<Map<String,String>> search_List = new ArrayList<>();
        Word signal_word;

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String sql = "select * from words where word like ? order by word desc";
        Cursor cursor = db.rawQuery(sql,new String[]{"%"+name+"%"});
        int column_word = cursor.getColumnIndex(Words.Word.COLUMN_NAME_WORD);
        int column_meaning = cursor.getColumnIndex(Words.Word.COLUMN_NAME_MEANING);
        int column_sentence = cursor.getColumnIndex(Words.Word.COLUMN_NAME_SAMPLE);
        if(cursor!=null){
            while(cursor.moveToNext()){
                Map<String,String> map = new HashMap<>();
                map.put(Words.Word.COLUMN_NAME_WORD,cursor.getString(column_word));
                map.put(Words.Word.COLUMN_NAME_MEANING,cursor.getString(column_meaning));
                map.put(Words.Word.COLUMN_NAME_SAMPLE,cursor.getString(column_sentence));
                search_List.add(map);
            }
            cursor.close();
        }
        return search_List;
    }

    //删除指定单词
    public int delete_Word(String strName){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        int result = db.delete(Words.Word.TABLE_NAME,"word = ?",new String[]{strName});
        return result;
    }

    //更新指定单词
    public int update_Word(Word word){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Words.Word.COLUMN_NAME_WORD,word.getName());
        values.put(Words.Word.COLUMN_NAME_MEANING,word.getMeaning());
        values.put(Words.Word.COLUMN_NAME_SAMPLE,word.getContence());
        int result = db.update(Words.Word.TABLE_NAME,values,"word=?",new String[] {word.getName()});
        return result;
    }
}
