package com.example.vocabulary_book;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Card_Activity extends AppCompatActivity {
    TextView tv_word,tv_meaning,tv_sentence;

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        tv_word = (TextView) findViewById(R.id.output_word);
        tv_meaning=(TextView) findViewById(R.id.output_meaning);
        tv_sentence=(TextView) findViewById(R.id.output_sentence);

        tv_word.setText(getIntent().getStringExtra(Words.Word.COLUMN_NAME_WORD));
        tv_meaning.setText(getIntent().getStringExtra(Words.Word.COLUMN_NAME_MEANING));
        tv_sentence.setText(getIntent().getStringExtra(Words.Word.COLUMN_NAME_SAMPLE));
    }
}