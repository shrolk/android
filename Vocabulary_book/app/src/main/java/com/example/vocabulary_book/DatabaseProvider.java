package com.example.vocabulary_book;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class DatabaseProvider extends ContentProvider {

    static private int wordsDir = 0;
    static private int wordsItem = 1;
    static private String authority = "com.example.vocabulary_book.provider.DatabaseProvider";
    private WordsDBHelper wordsDBHelper = null;
    static UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
    SQLiteDatabase db;

    static {
        matcher.addURI(authority, "words", wordsDir);
        matcher.addURI(authority, "words/#", wordsItem);
    }

    public DatabaseProvider() {

    }

    @Override
    public boolean onCreate() {
        // TODO: Implement this to initialize your content provider on startup.

        wordsDBHelper = new WordsDBHelper(getContext());
        return true;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        db = wordsDBHelper.getWritableDatabase();
        if(matcher.match(uri)==wordsDir){
            return db.delete(Words.Word.TABLE_NAME,selection,selectionArgs);
        }
        else if(matcher.match(uri)==wordsItem){
            String wordId = uri.getLastPathSegment();
            return db.delete(Words.Word.TABLE_NAME, Words.Word.COLUMN_NAME_WORD+"=?",new String[]{wordId});
        }
        return 0;
    }


    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        if(matcher.match(uri)==wordsDir)
            return "vnd.android.cursor.dir/vnd.com.example.vocabulary_book.provider."+ Words.Word.TABLE_NAME;
        else if(matcher.match(uri)==wordsItem)
            return "vnd.android.cursor.item/vnd.com.example.vocabulary_book.provider."+ Words.Word.TABLE_NAME;
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO: Implement this to handle requests to insert a new row.
        db = wordsDBHelper.getWritableDatabase();
        if (matcher.match(uri) == wordsItem || matcher.match(uri) == wordsDir) {
            Long newWordId = db.insert(Words.Word.TABLE_NAME, null, values);
            return Uri.parse("content://" + authority +"/"+ Words.Word.TABLE_NAME+"/"+ newWordId);
        } else
            return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        // TODO: Implement this to handle query requests from clients.

        db = wordsDBHelper.getReadableDatabase();

        if (matcher.match(uri) == wordsDir) {
            Cursor cursor = db.query(Words.Word.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
            return cursor;
        } else if (matcher.match(uri) == wordsItem) {
            String wordId = uri.getLastPathSegment();
            Cursor cursor1 = db.query(Words.Word.TABLE_NAME, projection, Words.Word.COLUMN_NAME_WORD + "=?", new String[]{wordId}, null, null, sortOrder);
            return cursor1;
        }

        return null;
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
       db = wordsDBHelper.getWritableDatabase();
       if(matcher.match(uri)==wordsDir){
           return db.update(Words.Word.TABLE_NAME,values,selection,selectionArgs);
       }
       else if(matcher.match(uri)==wordsItem){
           String wordId = uri.getPathSegments().get(1);
           return db.update(Words.Word.TABLE_NAME,values, Words.Word.COLUMN_NAME_WORD+"=?",new String[]{wordId});
       }
       return 0;
    }

}