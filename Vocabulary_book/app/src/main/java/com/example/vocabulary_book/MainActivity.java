package com.example.vocabulary_book;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    EditText et_Search, add_word, add_meaning, add_sentence;
    Button bt_Add, bt_Local_Search, bt_Online_Search;
    LinearLayout add_dialog;
    Control ct;
    RecyclerView recyclerView;
    private List word_list;
    private RecycleAdapter mRecyAdapter;
    RecyclerView.LayoutManager layoutManager;
    int view_num;
    RightFragment rightFragment;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_Search = (EditText) findViewById(R.id.EditText_Serach);
        bt_Add = (Button) findViewById(R.id.Button_add);
        bt_Local_Search = (Button) findViewById(R.id.Button_Local_Search);
        bt_Online_Search = (Button) findViewById(R.id.Button_Online_Search);
        add_dialog = (LinearLayout) getLayoutInflater().inflate(R.layout.add_dialog, null);
        add_word = (EditText) add_dialog.findViewById(R.id.Add_dialog_add);
        add_meaning = (EditText) add_dialog.findViewById(R.id.Add_dialog_meaning);
        add_sentence = (EditText) add_dialog.findViewById(R.id.Add_dialog_centence);
        ct = new Control(MainActivity.this);
        view_num = 0;

        rightFragment = (RightFragment) getSupportFragmentManager().findFragmentById(R.id.right_fragment);

        initDate();
        initEvent();

    }

    public void initEvent() {//配置器初始化
        recyclerView = findViewById(R.id.Main_RecyclerView);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mRecyAdapter = new RecycleAdapter(MainActivity.this, word_list,rightFragment);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mRecyAdapter);
    }

    public void initDate() {//载入数据
        word_list = new ArrayList<Map<String, String>>();
        word_list = ct.search_Word_All();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //本地查询
    public void Button_Local_Search(View view) {
        if (et_Search.getText().toString().length() == 0) {
            notNull();
        } else {
            String str = et_Search.getText()+"";
            if(ct.search_fuzzy(str.trim()).size()!=0){
                Toast.makeText(MainActivity.this, "查询成功", Toast.LENGTH_SHORT).show();
                word_list = ct.search_fuzzy(et_Search.getText()+"");
                initEvent();
            }
            else {
                Toast.makeText(MainActivity.this, "对不起，该单词不存在", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    //有道词典在线查询
    public void Button_Online_Search(View view) {
        if (et_Search.getText().toString().length() == 0) {
            notNull();
        }
    }


    //添加单词（利用对话框实现）
    public void Button_Add(View view) {
        AlertDialog.Builder add_Builder = new AlertDialog.Builder(this);
        add_Builder.setTitle("添加单词");

        ViewGroup parent = (ViewGroup) add_dialog.getParent();
        if (parent != null) {
            parent.removeView(add_dialog);
        }
        add_Builder.setView(add_dialog)
                .setPositiveButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (add_word.getText().toString().length() == 0 || add_meaning.getText().toString().length() == 0 || add_sentence.getText().toString().length() == 0)
                            notNull();
                        else {
                            Word word = new Word(add_word.getText() + "", add_meaning.getText() + "", add_sentence.getText() + "");
                            if (word.getName().equals(ct.search_Word(word.getName()).getName())) {
                                Toast.makeText(MainActivity.this, "单词已存在，请勿重复添加", Toast.LENGTH_SHORT).show();
                            } else {
                                int result = ct.add_Word(word);
                                if (result != -1) {
                                    Toast.makeText(MainActivity.this, "添加成功！", Toast.LENGTH_SHORT).show();
                                    update_showDate();
                                }
                                else
                                    Toast.makeText(MainActivity.this, "添加失败！", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                })
                .create()
                .show();//创建对话框
    }

    //输入不能为空
    public void notNull() {
        Toast.makeText(MainActivity.this, "文本输入不能为空，请重新输入", Toast.LENGTH_SHORT).show();
        return;
    }

    //刷新显示数据
    public void update_showDate(){
        initDate();
        initEvent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.help_item:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("帮助")
                .setMessage("这是帮助")
                        .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create()
                        .show();
        }
        return super.onOptionsItemSelected(item);
    }
}


