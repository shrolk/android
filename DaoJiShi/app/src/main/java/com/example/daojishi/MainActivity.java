package com.example.daojishi;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private TextView tvDay;
    private TextView tvHour;
    private TextView tvMinute;
    private TextView tvSecond;
    private TextView tvtime;

    private String selectText = "";

    private ArrayList<String> hourList = new ArrayList<>();
    private ArrayList<String> minuteList = new ArrayList<>();
    private ArrayList<String> dayList = new ArrayList<>();
    private ArrayList<String> secondList = new ArrayList<>();

    int num_day=0,num_hour=0,num_minute=0,num_second=0;
    int sum_time;
    double total_time;
    CountDownTime mTime;

    private CircleProgressBar circleProgressBar;
    private double totalProgress = 100;
    private double currentProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        initData();

        initListeners();
    }

    //初始化进度条
    private void initBar(){
        circleProgressBar = (CircleProgressBar) findViewById(R.id.circleProgressbar);
        new Thread(new ProgressRunable()).start();
    }

    private void initData() {
        // 填充列表
        dayList.clear();
        hourList.clear();
        minuteList.clear();
        secondList.clear();

        for (int i = 0; i <= 31; i++) {
            dayList.add(String.format("%d天", i));
        }
        for (int i = 0; i <= 23; i++) {
            hourList.add(String.format("%d小时", i));
        }
        for (int i = 0; i <= 59; i++) {
            minuteList.add(String.format("%d分钟", i));
            secondList.add(String.format("%d秒",i));
        }
    }

    private void initView() {
        tvDay = findViewById(R.id.tv_day);
        tvHour = findViewById(R.id.tv_hour);
        tvMinute = findViewById(R.id.tv_minute);
        tvSecond = findViewById(R.id.tv_second);
        tvtime = findViewById(R.id.time_remaining);
    }

    private void initListeners() {
        findViewById(R.id.tv_day).setOnClickListener(view -> showDialog(tvDay, dayList, 0)
        );
        findViewById(R.id.tv_hour).setOnClickListener(view -> showDialog(tvHour, hourList, 0));
        findViewById(R.id.tv_minute).setOnClickListener(view -> showDialog(tvMinute, minuteList, 0));
        findViewById(R.id.tv_second).setOnClickListener(view -> showDialog(tvSecond, secondList, 0));

    }

    private void showDialog(TextView textView, ArrayList<String> list, int selected) {
        showChoiceDialog(list, textView, selected,
                new WheelView.OnWheelViewListener() {
                    @Override
                    public void onSelected(int selectedIndex, String item) {
                        selectText = item;
                    }
                });
    }

    private void showChoiceDialog(ArrayList<String> dataList, final TextView textView, int selected,
                                  WheelView.OnWheelViewListener listener) {
        selectText = "";
        View outerView = LayoutInflater.from(this).inflate(R.layout.dialog_wheelview, null);
        final WheelView wheelView = outerView.findViewById(R.id.wheel_view);
        wheelView.setOffset(2);// 对话框中当前项上面和下面的项数
        wheelView.setItems(dataList);// 设置数据源
        wheelView.setSeletion(selected);// 默认选中第几项
        wheelView.setOnWheelViewListener(listener);

        // 显示对话框，点击确认后将所选项的值显示到Button上
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setView(outerView)
                .setPositiveButton("确认",
                        (dialogInterface, i) -> {
                            textView.setText(selectText);
                            textView.setTextColor(this.getResources().getColor(R.color.teal_200));
                        })
                .setNegativeButton("取消", null).create();
        alertDialog.show();
        int green = this.getResources().getColor(R.color.purple_200);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(green);
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(green);
    }

    //确定按钮
    public void Confirm_button(View View){
        String str = tvDay.getText()+"";
        if(Integer.parseInt(str.substring(0,str.length()-1))!=0){
            tvtime.setText(str+"");
            num_day = Integer.parseInt(str.substring(0,str.length()-1));
        }
        str = tvHour.getText()+"";
        if(Integer.parseInt(str.substring(0,str.length()-2))!=0){
            tvtime.setText(tvtime.getText()+str+"");
            num_hour = Integer.parseInt(str.substring(0,str.length()-2));
        }
        str = tvMinute.getText()+"";
        if(Integer.parseInt(str.substring(0,str.length()-2))!=0){
            tvtime.setText(tvtime.getText()+str+"");
            num_minute = Integer.parseInt(str.substring(0,str.length()-2));
        }
        str = tvSecond.getText()+"";
        if(Integer.parseInt(str.substring(0,str.length()-1))!=0){
            tvtime.setText(tvtime.getText()+str+"");
            num_second = Integer.parseInt(str.substring(0,str.length()-1));
        }
        sum_time = num_second+num_minute*60+num_hour*3600+num_day*216000;
        total_time = (double) sum_time;
    }

    //开始按钮
    public void Button_Choice(View view){
        mTime = new CountDownTime(sum_time*1000,1000);
        mTime.start();
        currentProgress = 0;
        initBar();
    }

    class CountDownTime extends CountDownTimer {

        //构造函数  第一个参数代表总的计时时长  第二个参数代表计时间隔  单位都是毫秒
        public CountDownTime(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {//每计时一次回调一次该方法
            tvtime.setText("");
            l = l/1000;
            if(l/216000>0){
                tvtime.setText(tvtime.getText()+""+l/216000 + "天");
                l = l%216000;
            }
            if(l/3600>0){
                tvtime.setText(tvtime.getText()+""+l/3600 + "小时");
                l = l%3600;
            }
            if(l/60>0){
                tvtime.setText(tvtime.getText()+""+l/60 + "分钟");
                l = l%60;
            }
            if(l>0){
                tvtime.setText(tvtime.getText()+""+l + "秒");
            }

        }

        @Override
        public void onFinish() { //计时结束回调该方法
            tvtime.setText("");
            AssetManager assetManager = getAssets();
            try {
                AssetFileDescriptor fd = assetManager.openFd("网络歌手 - 今天的不开心就止于此吧（海绵宝宝版） (铃声).mp3");
                MediaPlayer mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(fd.getFileDescriptor(),fd.getStartOffset(),fd.getLength());
                mediaPlayer.prepare();
                mediaPlayer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //进度条
    class ProgressRunable implements Runnable {
        @Override
        public void run() {
            while (currentProgress < totalProgress) {
                currentProgress += 100/total_time;
                if(currentProgress>100)
                    currentProgress = 100;
                circleProgressBar.setProgress(currentProgress);
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
