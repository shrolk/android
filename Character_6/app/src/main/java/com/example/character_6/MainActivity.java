package com.example.character_6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG="TAG";

    ServiceConnection mServiceConnection;
    CalcService mCalcService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                Log.v(TAG,"onServiceConnected");
                mCalcService = ((CalcService.LocalBinder) iBinder).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.v(TAG,"onServiceDisconnected");
            }
        };
        
    }
    public void onButtonStartServiceClick(View view){//启动服务
        Intent intent = new Intent(MainActivity.this,CalcService.class);
        bindService(intent,mServiceConnection, Service.BIND_AUTO_CREATE);
    }

    public void onButtonStopServiceClick(View view){//终止服务
        if(mCalcService!=null)
            unbindService(mServiceConnection);
    }

    public void onButtonAddClick (View view) {//使用服务的加法功能
        if(mCalcService!=null)
            Toast.makeText(MainActivity.this,"Using service add:"+mCalcService.add(10,5),Toast.LENGTH_LONG).show();
    }

    public void onButtonSubClick (View view) {//使用服务的减法功能
        if(mCalcService!=null)
            Toast.makeText(MainActivity.this,"Using service sub:"+mCalcService.sub(10,5),Toast.LENGTH_LONG).show();
    }
    public void onButtonChenClick (View view) {//使用服务的乘法功能
        if(mCalcService!=null)
            Toast.makeText(MainActivity.this,"Using service chen:"+mCalcService.chen(10,5),Toast.LENGTH_LONG).show();
    }

    public void onButtonChuClick (View view) {//使用服务的除法功能
        if(mCalcService!=null)
            Toast.makeText(MainActivity.this,"Using service chu:"+mCalcService.chu(10,5),Toast.LENGTH_LONG).show();
    }


}