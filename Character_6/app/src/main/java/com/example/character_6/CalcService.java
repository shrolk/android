package com.example.character_6;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

public class CalcService extends Service {
    private static final String TAG = "TAG";

    private LocalBinder mLocalBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        CalcService getService(){
            return CalcService.this;
        }
    }

    public int add(int x,int y){
        return x+y;
    }

    public int sub(int x,int y){
        return x-y;
    }

    public int chen(int x,int y){
        return x*y;
    }

    public int chu(int x,int y){
        return x/y;
    }

//    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG,"Service onBind");
        return mLocalBinder;
    }
}
