
package com.example.character_6_number;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.character_6_number.R;

public class MainActivity extends AppCompatActivity {

    EditText dt_input;
    TextView tv_output;
    Button bt_start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dt_input = (EditText) findViewById(R.id.Input);
        tv_output = (TextView) findViewById(R.id.Output);
        bt_start = (Button) findViewById(R.id.Bu_start);

        final Handler handler = new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                tv_output.setText(dt_input.getText()+""+msg.obj);
            }
        };

        final Runnable myWorker = new Runnable() {
            @Override
            public void run() {
                int number = Integer.parseInt(dt_input.getText()+"");
                int n = 0;
                Message msg = new Message();
                for(int i = 1;i<number/2;i++){
                    if(number%i==0)
                        n++;
                }
                if(n>1)
                    msg.obj = "不是素数";
                else
                    msg.obj = "是素数";
                handler.sendMessage(msg);
            }
        };

        bt_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread workThread = new Thread(null,myWorker,"Worker");
                workThread.start();
            }
        });
    }
}
