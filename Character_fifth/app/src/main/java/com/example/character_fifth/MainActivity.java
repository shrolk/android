package com.example.character_fifth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    //SharedPreference文件名
    private final static String SharedPreferencesFileName="config";

    //键
    private final static String Key_UserName = "UserName";//用户名
    private final static String Key_LoginDate = "LoginDate";//登录时间
    private final static String Key_UserType = "UserType";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //获得SharedPreferences实例子
        preferences = getSharedPreferences(SharedPreferencesFileName,MODE_PRIVATE);
        editor=preferences.edit();
        Button btnwrite = (Button)findViewById(R.id.ButtonWrite);
        Button btnRead = (Button) findViewById(R.id.ButtonRead);

        btnwrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //格式化日期，将日期按照年月日时分秒格式转换为字符串格式
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String strLoginDate = simpleDateFormat.format(new Date());//获取当前日期和时间

                //写入键值对
                editor.putString(Key_UserName,"hdsoad");
                editor.putString(Key_LoginDate,strLoginDate);
                editor.putInt(Key_UserType,2019011410);
                editor.apply();
            }
        });
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strUserName = preferences.getString(Key_UserName,null);
                String strLoginDate = preferences.getString(Key_LoginDate,null);
                int nUserType = preferences.getInt(Key_UserType,0);
                if(strUserName!=null&&strLoginDate!=null)
                    Toast.makeText(MainActivity.this,"用户名:"+strUserName+"  学号:"+nUserType+"登录时间"+strLoginDate,Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(MainActivity.this,"无数据",Toast.LENGTH_LONG).show();
            }
        });
    }


}